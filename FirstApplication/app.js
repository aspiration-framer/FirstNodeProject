
/* Declaration */
var express = require('express');
var app = express();
var http = require('http').Server(app);
var fs = require('fs');
var bodyParser = require('body-parser');

/* Common declaration */
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });

/* using */
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/* Error Handling */
app.use(function(err, req, res, next) {
  console.error("ErrorMessage : "+err.stack + "\n ErrorStatus : "+err.status);
  var data = {   ErrorStatus : err.status , ErrorMessage : err.stack };
  res.status(500).send(JSON.stringify(data));
});


/* Home page call */
app.get('/',function(req,res){

	fs.readFile('views/index.html',function(err,data){
		res.writeHead(200,{ 'Content-Type':'text/html'});
		res.write(data);
		res.end();
	});
});

/* About page call */
app.get('/about',function(req,res){

	fs.readFile('views/about.html',function(err,data){
		res.writeHead(200,{ 'Content-Type':'text/html'});
		res.write(data);
		res.end();
	});
});

/* Services page call */
app.get('/services',function(req,res){

	fs.readFile('views/services.html',function(err,data){
		res.writeHead(200,{ 'Content-Type':'text/html'});
		res.write(data);
		res.end();
	});
});

/* Contact page call */
app.get('/contact',function(req,res){

	fs.readFile('views/contact.html',function(err,data){
		res.writeHead(200,{ 'Content-Type':'text/html'});
		res.write(data);
		res.end();
	});
});


/* Get Methods */
app.get('/sendGetQuery/:id',function(req,res){
	console.log('Get Method : '+req.params);
	var data = { id:req.params.id,name:'Mohammad Rajpurwala'};
	res.send(JSON.stringify(data));
});

/* Post Methods */
app.post('/sendPostQuery',function(req,res){
	console.log('Post Method : '+ JSON.stringify(req.body));
	var data = { id:req.params.id,name:'Mohammad Rajpurwala'};
	res.send(JSON.stringify(data));
});

app.post('/login',urlencodedParser,function(req,res){
	console.log('Post Method : '+JSON.stringify(req.body));
	var data = {id:req.body.id , name:'Mohammad Rajpurwala'};
	res.send(JSON.stringify(data));
});


app.post('/api/login',jsonParser,function(req,res){
	console.log('Post Method : '+JSON.stringify(req.body));
	var data = {id:req.body.id , name:'Mohammad Rajpurwala'};
	res.send(JSON.stringify(data));
});



/* Listen port */
app.listen(3000);

console.log('Server Started at port : 3000');